# Zendesk Organization Stats

Pull some stats from Zendesk for a specific organization and visualize them.

## Setup

```pip install jupyter pandas wordcloud matplotlib```

## Getting the data

If you don't have your password or token handy, use the [Zendesk API console](https://developer.zendesk.com/requests/new) and authorize against your subdomain.

Then call `/organizations/$organization_id/tickets.json?include=metric_events` setting $organization_id to the ID of the organization you want to report on.

Safe the resulting json as `tickets.json`. Run the notebook and execute the cells.
